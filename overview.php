<?php
/*
Template Name: Overview
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section overview-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section section-light chain-section text-center">
        <div class="container">
            <h2 class="section-title">Don’t just react to supply chain challenges. <br> Conquer them.</h2>
            <p class="section-lead"> You’re under constant pressure to reduce your freight spend. But that’s tough to do when you can’t see the decisions partners are making on your behalf until well after the fact. A big challenge is that suppliers and carriers don’t have a way to connect to you through their own systems and processes—severely limiting your visibility. How can you monitor all their transactions—and intervene quickly when they don’t meet your standards? </p>
        </div>
    </section>
    <!-- begin section why-section section-light-gray -->
    <section class="section why-section section-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6 col-lg-3 col-lg-push-7 col-lg-offset-1"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/why.svg" alt="" class="img-responsive section-image-vcenter"> </div>
                <div class="col-md-6 col-md-pull-6 col-lg-7 col-lg-pull-4">
                    <h3>With Agistix.</h3>
                    <p>Our modern platform is designed to work with older solutions and adapt easily to changes in your infrastructure. You can extend your reach without burdening IT or changing processes—for you or your partners. </p>
                    <h3>The Result?</h3>
                    <p>It’s suddenly easy to see transactional data for all orders and shipments in your supply chain. That means you can be proactive about savings opportunities—and easily measure the impact of your work. </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end section why-section section-light-gray -->
    <section class="section section-light samt-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="img-block tablet"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/tablet.png" alt="" class="img-responsive"> </div>
                    <div class="img-block iphone"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/iphone.png" alt="" class="img-responsive"> </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="overview-list">
                        <h4>See all transactions from all partners</h4>
                        <p>No matter what technology your providers and customers are using, Agistix gives you a single command center for all transactional data. This includes all inbound and outbound orders and shipments, all modes, all service levels, and all geographies.</p>
                        <h4>Anticipate and fix issues in real time</h4>
                        <p>Agistix delivers aggregated data and automatic alerts that let you see—and address—areas of concern before they develop into costly problems. You no longer have to wait to audit compliance, manage exceptions, increase efficiency, deal with disruption, or reduce costs.</p>
                        <h4>Measure and quantify your supply chain performance</h4>
                        <p>With Agistix, you get the numbers you need to prioritize your supply chain improvements. You can then measure the exact results of your changes—and let your stakeholders access this data through self-service, if you chose.</p>
                        <h4>Total visibility at your fingertips</h4>
                        <p>With hundreds or thousands of shipments in progress at any moment, you simply can't monitor everything as it happens. Agistix does that for you, delivering at-a-glance real-time tracking for all your shipments.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>