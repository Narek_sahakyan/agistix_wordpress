<?php
/*
Template Name: Technology
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section technology-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section section-light text-center">
        <div class="container">
            <h2 class="section-title">Upgrade your supply chain technology—<br>without calling IT.</h2>
            <p class="section-lead">Traditional vendors address complex supply chain challenges with complex technology. We take a different approach. We deliver a solution that adapts to your processes. We offer expert guidance to help you go live quickly. And we let you achieve the ongoing time and cost savings of the cloud. That means you and your partners can work the way you’ve always worked—and
                <br> easily achieve your goal of ecosystem-wide visibility.</p>
        </div>
    </section>
    <section class="section section-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/gear.svg" alt="" class="img-responsive"> </div>
                <div class="col-md-7 col-md-offset-1 col-lg-6 col-lg-offset-2">
                    <h3>Using legacy technology? No problem.</h3>
                    <p>You and your partners probably use a wide range of technologies—some sophisticated, some not. That’s OK. Agistix can talk to your systems without heavy integration. In fact, you can get started with Agistix even if you don't have a TMS installed. We’ll use EDI, APIs, and other approaches to connect to your partners—and deliver the complete, aggregated data you need to drive results.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-light catchup-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h3>Tired of playing catch-up? We’ll keep you ahead of the curve.</h3>
                    <p>Constant changes in the market force you to remain agile. With Agistix, you can get new partners up and running quickly. Our intuitive interface lets you change controls without IT involvement. And our APIs make it easy to use your newly aggregated data in ways that drive even more value across your organization.</p>
                </div>
                <div class="col-lg-3 col-lg-offset-2"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/iphone.png" alt="" class="img-responsive"> </div>
            </div>
        </div>
    </section>
    <section class="section section-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/cloud.svg" alt="" class="img-responsive"> </div>
                <div class="col-md-7 col-md-offset-1 col-lg-6 col-lg-offset-2">
                    <h3>Save time and money in the cloud. We’ll show you how.</h3>
                    <p>With Agistix’s SaaS platform, all your data normalization, integration, architecture, and services are provided in the cloud. This can dramatically reduce your IT workload and costs. And no matter where you or your partners may be, you’ll have access to Agistix at any time, from any web-enabled device.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-light text-center">
        <div class="container">
            <h2 class="section-title">Carriers changed their systems? Don’t worry.</h2>
            <p>Today’s carriers are constantly tweaking their technology—presenting your IT staff with a lot of extra integration work on short notice. Agistix keeps you one step ahead. Because our system requires only minimal integration with other platforms, you’ll be able to keep up without even calling IT. </p>
            <br>
            <br> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/mac.png" alt="" class="img-responsive img-center"> </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>