<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="telephone=no" name="format-detection">
    <!-- This make sence for mobile browsers. It means, that content has been optimized for mobile browsers -->
    <meta name="HandheldFriendly" content="true">
    <link rel="image_src" href="http://agistix.comstatic/img/general/banner/home.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo( get_template_directory_uri() ); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo( get_template_directory_uri() ); ?>/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo( get_template_directory_uri() ); ?>/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo( get_template_directory_uri() ); ?>/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo( get_template_directory_uri() ); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Agistix">
    <meta name="application-name" content="Agistix">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
    <script>
        ! function(e) {
            e.className = e.className.replace(/\bno-js\b/, "js")
        }(document.documentElement)
    </script>          
</head>

<body class="page navbar-fixed">
    <!-- header-navbar -->
    <header class="header-navbar navbar-fixed-top navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarMain-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar top"></span> <span class="icon-bar middle"></span> <span class="icon-bar bottom"></span> </button>
                <a class="navbar-brand" href="<?php echo( home_url( '/' ) ); ?>"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/logo.svg" height="40" alt="Agistix"> </a>
            </div>
            <div class="collapse navbar-collapse" id="navbarMain-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown" data-trigger="hover"> <a href="#" data-toggle="dropdown" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Solutions</a>
                        <a href="<?php echo( home_url( '/overview/' ) ); ?>" class="dropdown-link hidden-xs hidden-sm"></a>
                        <ul class="dropdown-menu dropdown-menu-center">
                            <li><a href="<?php echo( home_url( '/overview/' ) ); ?>">Overview</a></li>
                            <li><a href="<?php echo( home_url( '/what-to-expect/' ) ); ?>">What to Expect</a></li>
                            <li><a href="<?php echo( home_url( '/fast-implement/' ) ); ?>">Fast implementation</a></li>
                        </ul>
                    </li>
                    <li class=""><a href="<?php echo( home_url( '/technology/' ) ); ?>">Technology</a></li>
                    <li class=""><a href="<?php echo( home_url( '/resources/' ) ); ?>">Resources</a></li>
                    <li class=""><a href="<?php echo( home_url( '/about/' ) ); ?>">About Us</a></li>
                    <li class=""><a href="<?php echo( home_url( '/contact/' ) ); ?>">Contact Us</a></li>
                    <li><span class="navbar-divider"></span></li>
                    <li><a href="//app.agistix.com/fs/servlet/LS?action=GetLogin" class="navbar-link">Login</a></li>
                </ul>
            </div>
        </div>
    </header>
    <!-- end header-navbar -->
    <!-- page-wrapper -->
    <div class="page-wrapper">