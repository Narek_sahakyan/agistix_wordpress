<?php
get_header();
?>
<section class="section section-light text-center">
    <div class="container">
        <h2 class="section-title">Sorry, the page you are looking for is not found.</h2> </div>
</section>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>