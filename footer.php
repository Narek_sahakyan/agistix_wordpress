        <div class="page-buffer"></div>
    </div>
    <!-- end page-wrapper -->
    <!-- page-footer -->
    <footer class="page-footer">
        <!-- page-footer -->
        <div class="container">
            <!-- begin footer-menu -->
            <ul class="footer-menu">
                <li><a href="<?php echo( home_url( '/overview/' ) ); ?>">Solutions</a></li>
                <li><a href="<?php echo( home_url( '/technology/' ) ); ?>">Technology</a></li>
                <li><a href="<?php echo( home_url( '/resources/' ) ); ?>">Resources</a></li>
                <li><a href="<?php echo( home_url( '/about/' ) ); ?>">About Us</a></li>
                <li><a href="<?php echo( home_url( '/contact/' ) ); ?>">Contact Us</a></li>
                <li><a href="//app.agistix.com/fs/servlet/LS?action=GetLogin">Login</a></li>
            </ul>
            <!-- end footer-menu -->
        </div>
        <!-- end page-footer -->
        <!-- begin copyright -->
        <div class="copyright">
            <div class="container-fluid">
                <p>+1-888-244-7849 or <a href="mailto:info@agistix.com">info@agistix.com</a> 177 Bovet Road, Suite 110, San Mateo, CA 94402 USA</p>
                <p> &copy; 2017 Agistix Inc. All rights reserved. <a href="<?php echo( home_url( '/privacy/' ) ); ?>">Privacy Policy</a></p>
            </div>
        </div>
        <!-- end copyright -->
    </footer>
    <!-- end page-footer -->
    <?php
    if ( is_page_template( 'resources.php' ) ) {
    ?>
        <!-- Modal -->
        <div class="modal fade" id="reqPdfModal" tabindex="-1" role="dialog" aria-labelledby="reqPdfLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="reqPdfLabel">Get this item by filling in the form</h4> </div>
                    <div class="modal-body">
                        <form class="contact-form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" name="contact_form" id="reqPdfForm">
                            <input type="hidden" name="oid" value="00DE0000000Iufz">
                            <input type="hidden" name="retURL" id="retURL" value="http://www.agistix.com/resources?status=success">
                            <input type="hidden" name="lead_source" value="Website - Resources Page">
                            <input type="hidden" name="Campaign_ID" value="701E00000004oxl">
                            <div class="form-group">
                                <label for="first_name">First Name <span class="req">*</span></label>
                                <input class="form-control" required id="first_name" name="first_name"> </div>
                            <div class="form-group">
                                <label for="last_name">Last Name <span class="req">*</span></label>
                                <input class="form-control" required id="last_name" name="last_name"> </div>
                            <div class="form-group">
                                <label for="company">Company <span class="req">*</span></label>
                                <input class="form-control" required id="company" name="company"> </div>
                            <div class="form-group">
                                <label for="email">Email <span class="req">*</span></label>
                                <input type="email" class="form-control" required id="email" name="email"> </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">Send</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }

    if ( is_front_page() ) {
    ?>
        <script src="<?php echo( get_template_directory_uri() ); ?>/static/js/homead0yho.min.js"></script>
    <?php
    } elseif ( is_page_template( 'resources.php' ) ) {
    ?>
        <script src="//calendly.com/assets/external/widget.js" type="text/javascript"></script>
        <script src="<?php echo( get_template_directory_uri() ); ?>/static/js/resourcesad0yho.min.js"></script>
    <?php
    } elseif ( is_page_template( 'trial-offer.php' ) ) {
    ?>
        <script src="<?php echo( get_template_directory_uri() ); ?>/static/js/trialOfferad0yho.min.js"></script>
    <?php
    } elseif ( is_page_template( 'contact.php' ) ) {
    ?>
        <script src="<?php echo( get_template_directory_uri() ); ?>/static/js/contactad0yho.min.js"></script>
    <?php
    } else {
    ?>
        <script src="<?php echo( get_template_directory_uri() ); ?>/static/js/mainad0yho.min.js"></script>
    <?php
    }
    ?>
    <script src="https://www.google-analytics.com/urchin.js" type="text/javascript"></script>
    <script type="text/javascript">
        _uacct = "UA-295693-1", urchinTracker(), llactid = 15003
    </script>
    <script type="text/javascript">
        var sf14gv = 15003;
        ! function() {
            var t = document.createElement("script");
            t.type = "text/javascript", t.async = !0, t.src = ("https:" == document.location.protocol ? "https://" : "http://") + "t.sf14g.com/sf14g.js";
            var e = document.getElementsByTagName("script")[0];
            e.parentNode.insertBefore(t, e)
        }()
    </script>
    <?php wp_footer(); ?>
</body>

</html>