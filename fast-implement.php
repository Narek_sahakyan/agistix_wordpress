<?php
/*
Template Name: Fast Implementation
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section fastImplement-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section get-result-section section-light text-center">
        <div class="container">
            <h2 class="section-title">Get results in four to six weeks.</h2>
            <p class="section-lead">Hundreds or even thousands of shippers, carriers, suppliers and partners in your ecosystem make communication challenging. We think your supply chain solution should make your job simpler. That’s why we built Agistix differently. We designed our solution to target the highest ROI by automating the day-to-day processes so that you’ll have more time to manage exceptions and focus on strategy. We then built our solution to deliver value as quickly as possible. You’ll see results in weeks—not the months or years required to implement typical supply chain solutions.</p>
        </div>
    </section>
    <section class="section section-light-gray flag-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-2"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/flag.svg" alt="" class="img-responsive section-image-vcenter"> </div>
                <div class="col-lg-8 col-lg-offset-1">
                    <h3>We love helping companies succeed</h3>
                    <p>Our proven methodology delivers results for the world’s most powerful companies. Our team combines deep experience and passion for continuously refining best practices to lead you through all stages of your implementation.</p>
                    <h3>We’ll do the heavy lifting...</h3>
                    <p>Your IT team won’t even need to get involved in implementing Agistix. In fact, your staff’s total time commitment will be minimal. Just let us know which carriers to work with and we’ll make total visibility a reality.</p>
                    <h3>...But you’ll be in the driver’s seat</h3>
                    <p>Although we do most of the technical work, you’re in control. We’ll work with your team to identify your priorities—and we won’t move on until we’ve demonstrated results. This approach ensures quality results that meet your goals</p>. </div>
            </div>
        </div>
    </section>
    <section class="section section-light text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="img-block images-3d"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/iphone.png" alt="" class="img-responsive iphone"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/tablet-ship-manager.png" alt="" class="img-responsive tablet-middle"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/tablet.png" alt="" class="img-responsive tablet-big"> </div>
                </div>
            </div>
            <h2 class="section-title">Your new system will keep up with you</h2>
            <p class="section-lead">As your supply chain evolves, so do your needs. Our approach makes it easy to expand your use of Agistix in ways that address your biggest business challenges.</p>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>