<?php
/*
Template Name: About Us
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section about-us-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section section-light text-center overflow">
        <div class="container">
            <h2 class="section-title">If you could see everything, what could you achieve?</h2>
            <p class="section-lead">Sales, customers, suppliers, finance, your leadership team: they all expect you to track the progress of thousands of shipments moving from point to point around the globe. They don’t care that you’re dealing with an increasingly complex web of suppliers. They just expect you to get the job done—and to reduce your spend while you’re at it.</p>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="img-block flying-icons-wrapper">
                        <div class="svg-wrapper hidden-xs"> <img class="gear" width="108px" height="72px" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/svg-icons/gear.svg"> <img class="tooltip" width="77px" height="60px" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/svg-icons/tooltip.svg"> <img class="note" width="87px" height="68px" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/svg-icons/note.svg"> <img class="rounds" width="79px" height="72px" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/svg-icons/rounds.svg"> <img class="calendar" width="70px" height="87px" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/svg-icons/calendar.svg"> </div> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/tabletLandscape.png" alt="" class="img-responsive tablet-landscape"> </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-light-gray text-center">
        <div class="container">
            <h3 class="section-title">We know supply chain</h3>
            <p>The only people who really understand the challenges of freight management are the people who have actually faced them daily. Agistix was founded by a team of logistics and technology experts who have a passion for good data. We know how much potential is hidden in most transportation companies data—and we‘ve spent our careers delivering solutions that enable operational best practices and process improvements.</p>
            <br>
            <h3 class="section-title">We saw a need</h3>
            <p>Companies with complex supply chains often lack visibility into their shipments across different systems, carriers, suppliers and modes. So we built data and analytic tools that work with a companys existing systems to give them deep insight into what's really happening across their entire supply chain. The result? A breakthrough in how freight is managed.</p>
            <br>
            <h3 class="section-title">We reveal the opportunity hidden in data</h3>
            <p>Agistix customers aren‘t overwhelmed by the rising volume of data—they‘re using it to their advantage. They’re tapping into previously hidden opportunities to control shipping rates, negotiate better contracts, identify fraud—and establish stronger, more strategic supply chains. Companies such as General Dynamics, Cummins, and XO Communications are doing just that.</p>
        </div>
    </section>
    <section class="section section-light">
        <div class="section-half-backdrop left hidden-xs" style="background-image:url(<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/shine.png)"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-lg-6 col-lg-offset-0">
                    <ul class="awards-wrapper img-block list-unstyled">
                        <li>
                            <a href="http://www.inboundlogistics.com/cms/top-100-lit/"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/top-100-logo-17.png" alt="" width="144" height="75"></a>
                        </li>
                        <li>
                            <a href="http://www.inboundlogistics.com/cms/top-100-lit/"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/top-100-logo-18.png" alt="" width="144" height="75"></a>
                        </li>
                        <li>
                            <a href="https://www.sdcexec.com/warehousing/article/12308681/meet-the-2017-pros-to-know" target="_blank"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/pros-to-know.png" alt="" width="123" height="123"></a>
                        </li>
                        <li>
                            <a href="https://www.sdcexec.com/awards/article/20993474/meet-the-2018-pros-to-know" target="_blank"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/pros-to-know-18.png" alt="" width="123" height="123"></a>
                        </li>
                        <li>
                            <a href="http://resources.inboundlogistics.com/digital/lit_top100_2016.pdf" target="_blank"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/100-executive.png" alt="" width="145" height="116"></a>
                        </li>
                        <li>
                            <a href="http://www.sdcexec.com/article/12340659/laying-the-framework-for-supply-chain-success" target="_blank"><img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/SDC100Logo_2017.png" alt="" width="145" height="116"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <h3 class="section-half-title">The world is taking notice</h3>
                    <p>By delivering a new level of visibility—and efficiency—to the supply chain, Agistix has grown to become the respected partner of leading companies across industries. Supply chain publications have affirmed our vision by honoring us with a wide range of prestigious awards:</p>
                    <dl class="dl-default"> <dt>Inbound Logistics:</dt>
                        <dd> <a href="http://www.inboundlogistics.com/cms/top-100-lit/" target="_blank">2016 Top 100 Logistics IT Provider</a>
                            <br> <a href="http://www.inboundlogistics.com/cms/top-100-lit/" target="_blank">2017 Top 100 Logistics IT Provider</a>
                            <br> <a href="http://www.inboundlogistics.com/cms/top-100-lit/" target="_blank">2018 Top 100 Logistics IT Provider</a> </dd> <dt>Silicon Review:</dt>
                        <dd> <a href="http://thesiliconreview.com/magazines/50-fastest-growing-tech-companies-2016/" target="_blank">50 Fastest Growing Tech Companies</a> </dd> <dt>Supply & Demand Chain Executive: <br><small>- An annual listing highlighting 100 great supply chain projects</small></dt>
                        <dd> <a href="http://resources.inboundlogistics.com/digital/lit_top100_2016.pdf" target="_blank">2016 SDCE 100</a>
                            <br> <a href="http://www.sdcexec.com/article/12340659/laying-the-framework-for-supply-chain-success" target="_blank">2017 SDCE 100</a> </dd> <dt>Supply & Demand Chain Executive:</dt>
                        <dd> <a href="http://www.sdcexec.com/news/12170714/meet-the-2016-pros-to-know" target="_blank">2016 Pros to Know</a>
                            <br> <a href="http://www.sdcexec.com/article/12308681/meet-the-2017-pros-to-know" target="_blank">2017 Pros to Know</a>
                            <br> <a href="https://www.sdcexec.com/awards/article/20993474/meet-the-2018-pros-to-know" target="_blank">2018 Pros to Know</a> </dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-light-gray careers-section">
        <div class="container">
            <h3>Careers at Agistix</h3>
            <p>Agistix is growing! Are you excited about supply chain optimization? About using technology to drive true breakthrough? Here is your chance to make a difference. We're seeking team members that want to be a part of something special and are looking for a career working with a leading edge team and customers that include some of the most respected companies in their industries. We are currently recruiting for Senior Software Engineers, UI Engineers and Account Managers. If you are interested, send your resume to careers@agistix.com.</p>
            <div class="panel-group" id="careersAccordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#accountSupervisor" aria-expanded="true" aria-controls="accountSupervisor">Account Supervisor</a> </h4> </div>
                    <div id="accountSupervisor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul class="list-styled">
                                <li>Learn our software through hands-on training, using it as a customer would, in designated test scenarios until you are proficient and able to test the Perform a variety of data support tasks, including develop custom reports, analyze customer data to identify trends and data anomalies, and data scrubbing </li>
                                <li>Input customer account information, review and maintain account databases, setup new customers</li>
                                <li>Enter invoices and contact vendors/customers/carriers for verification</li>
                                <li>Respond to client’s requests for information or account changes</li>
                                <li>Monitor our EDI orchestrations for files that need reprocessing and contact customers when needed </li>
                                <li>Test new application enhancements, identify and report issues in a timely manner, create and update project management tickets</li>
                                <li>Assist with troubleshooting by researching in depth, testing and assessing, and then reporting specifics with facts and detailed examples</li>
                                <li>Work and collaborate as a strong team member with all areas of our business – our development engineers, customer support, sales</li>
                                <li>Updated and create training information</li>
                                <li>Conduct online training sessions with customer, their suppliers and carriers </li>
                                <li>Create change orders for new customer initiatives that are outside of initial statement of work</li>
                                <li>Must obtain a static IP address to support work from home access to our FTP servers</li>
                                <li>The majority of this position will be focused on supporting a single customer</li>
                                <li>Occasional face to face meeting with customer in the SF Bay Area (anticipate less than once a month, meetings are generally in Concord or San Ramon)</li>
                                <li>Occasional time in our office in San Mateo</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>Strong Microsoft Excel skills</li>
                                <li>Outstanding computer skills</li>
                                <li>General knowledge of EDI </li>
                                <li>Self-motivated - sets priorities and takes initiative with given assignments</li>
                                <li>Detail oriented - corrects errors with precision, questions with insight and thoroughness, examines fine points for accuracy</li>
                                <li>Excellent written and verbal communication skills</li>
                                <li>Keen sense of timeliness</li>
                                <li>Comfortable with minimal supervision, but high accountability</li>
                                <li>Ability to switch tasks quickly and sometimes often</li>
                                <li>Willingness to learn as you go, with a “Do-It-Yourself” attitude</li>
                                <li>Highly organized - can locate files, emails, presentations quickly</li>
                                <li>Current computer system (PC or Mac), secure high speed internet, and workspace free from distractions and noise</li>
                                <li>A strong work ethic with a positive attitude, a team player with high integrity. We all work hard at Agistix, we treat each other with respect, and together we make a great team. We want the newest member of our team to have the same qualities </li>
                            </ul>
                            <h5>Additional Details</h5>
                            <ul>
                                <li>
                                    <h6>Ongoing training</h6>
                                    <ul>
                                        <li>Conduct regular training webinars</li>
                                        <li>Update training materials</li>
                                        <li>Provide information on new functionality</li>
                                    </ul>
                                </li>
                                <li>
                                    <h6>Metrics Management</h6>
                                    <ul>
                                        <li>% of fleet shipments booked on Agistix forms</li>
                                        <li>Number of shipments created from invoices</li>
                                        <li>Number of supplier shipments booked in Agistix weekly</li>
                                        <li>Supplier support activity (number of calls and emails, number of new users added)</li>
                                        <li>Monthly accrual report – estimated charges for shipments not yet invoiced</li>
                                    </ul>
                                </li>
                                <li>
                                    <h6>Supplier Booking Initiative</h6>
                                    <ul>
                                        <li>Identification of suppliers that should be booking in Agistix</li>
                                        <li>Supplier training</li>
                                        <li>New user creation</li>
                                        <li>Ongoing support</li>
                                        <li>Identification of suppliers not creating shipments from PO, and re-training</li>
                                    </ul>
                                </li>
                                <li>
                                    <h6>Carrier Oversight</h6>
                                    <ul>
                                        <li>Timely shipment updates</li>
                                        <li>Accurate EDI data</li>
                                    </ul>
                                </li>
                                <li>
                                    <h6>Support of Strategic Initiatives</h6>
                                    <ul>
                                        <li>Continued Supplier Onboarding</li>
                                        <li>Reinitiate discussion for supplier id integration from SAP</li>
                                        <li>Sub-supplier visibility to PO’s – major initiative because requires data from suppliers to know which supplier should have visibility to which PO’s and line items of PO’s</li>
                                        <li>ASN creation</li>
                                        <li>Rollout of poles shipping</li>
                                        <li>Other initiatives as PG&E needs dicatate</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h5 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#analysisIntern" aria-expanded="true" aria-controls="analysisIntern">Supply Chain Analysis Intern </a> </h5> </div>
                    <div id="analysisIntern" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>Monitor integration feeds for data integrity and continuity</li>
                                <li>Correlate data across disparate data sources to validate data accuracy</li>
                                <li>Analyze & recognize trends in customer & carrier data – rates, transit status, etc</li>
                                <li>Troubleshoot data irregularities and data source connectivity issues</li>
                                <li>Develop recommendations to streamline customer shipping based on data analysis</li>
                                <li>Support testing of new integrations, uses cases, and software enhancements</li>
                                <li>Complete tasks associated with customer & carrier implementations</li>
                                <li>Administrative tasks associated with maintaining master account data</li>
                                <li>Reporting & analytics support to present customer and carrier data</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>Incredible attention to detail</li>
                                <li>Strong computer skills, Excel, and database familiarity a plus</li>
                                <li>An outstanding work ethic, high-integrity, team player in attitude and initiative</li>
                                <li>Desire to work with Big Data, curious, and resourceful</li>
                                <li>Experience PowerPoint, visualization tools, and other presentation formats</li>
                                <li>Excellent written and verbal communication skills</li>
                                <li>Understanding of supply chain, logistics and shipping networks is a plus</li>
                                <li>Project management experience preferred</li>
                                <li>Comfortable with “hands-on” and “do-it-yourself” tactical efforts</li>
                            </ul>
                            <h5>Educational Requirement</h5>
                            <ul>
                                <li>Undergraduate degree</li>
                                <li>MBA student, preferably in Operations Management, Supply Chain Management,</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#seniorDirTech" aria-expanded="true" aria-controls="seniorDirTech">Senior Director of Technology</a> </h4> </div>
                    <div id="seniorDirTech" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>Monitor inbound email & calls to support customer, carrier and/or supplier questions</li>
                                <li>Basic troubleshooting to determine if issues could be browser, network, or our application</li>
                                <li>Create trouble tickets for any bugs, monitor ticket status and communicate back to client (call or email)</li>
                                <li>Develop strong understanding of our platform to ultimately help train new users and/or suppliers</li>
                                <li>Support application testing during low call volume or trouble ticket activity running through use cases and regression testing</li>
                                <li>Help consolidate and maintain knowledge base in Zendesk</li>
                                <li>Communication and hand-off of open tasks to ensure customer continuity</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>Reliable, high-speed internet connection</li>
                                <li>Incredible written & verbal communication skills, good listener and phone disposition</li>
                                <li>Good troubleshooter and attention to detail</li>
                                <li>Strong computer skills, appreciation for browser challenges, Skype, and basic understanding of the internet</li>
                                <li>An outstanding work ethic, high-integrity, team player in attitude and initiative</li>
                                <li>Self-starter, hungry for knowledge, and resourceful</li>
                                <li>Experience with software applications, salesforce & zendesk a plus</li>
                                <li>Understanding of supply chain, logistics and shipping networks is a plus</li>
                                <li>Comfortable with “hands-on” and “do-it-yourself” tactical efforts</li>
                                <li>Flexible hours between 8 am EST - 2 pm EST</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#ediBusinessAnalyst1" aria-expanded="true" aria-controls="ediBusinessAnalyst1">EDI Business Analyst</a> </h4> </div>
                    <div id="ediBusinessAnalyst1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <p>Experience working with EDI, specifically ~ 110, 204, 210, 211, 212, and 214 schema </p>
                            <ul>
                                <li>Familiarity with web services and REST APIs</li>
                                <li>Understanding and appreciation for systems integration and data normalization</li>
                                <li>Comfortable with FTP, sFTP, AS2 and other data exchange protocols</li>
                                <li>Active development & testing with offshore integration team to ensure functional integrity while working with onshore team for business requirements</li>
                                <li>Help create Trading Partner EDI and business requirements into a functional design</li>
                                <li>Define integration testing use cases and work with offshore teams to ensure data integrity</li>
                                <li>Participate in developer process and production release to ensure platform functionality is deployed with integration requirements</li>
                                <li>Investigate, validate and prioritize resolution of reported bugs or integration failures</li>
                                <li>Review and contribute to product release documentation</li>
                                <li>Participates in requirements prioritization, release scope control and management</li>
                                <li>Help with regression testing of common use cases as product release occur</li>
                                <li>Help innovate in the development of new/improved methods or approaches to problems</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>Exposure to and understanding of key business processes associated with Transportation Electronic Data Interchange (EDI)</li>
                                <li>Experience with Enterprise Resource Planning (ERP) solutions.</li>
                                <li>Excellent communication/presentation skills.</li>
                                <li>Systematic and logical approach to problem-solving, analysis and testing.</li>
                                <li>Extensible Markup Language (XML)</li>
                                <li>Use-case modeling and design documentation skills.</li>
                                <li>An appreciation of relevant technologies, to include application development environments and languages, databases, networking and client/server systems.</li>
                                <li>Demonstrate ability to think laterally: by recommending innovative enhancements, effective solutions to customer requirements/faults, etc.</li>
                                <li>Experience with business object modeling tools and/or entity relationship modeling tools.</li>
                                <li>A working knowledge of modern software engineering practices and methodologies.</li>
                                <li>General office environment.</li>
                                <li>Moderate levels of stress may occur at times.</li>
                                <li>No special physical demands required.</li>
                                <li>Ability to travel internationally.</li>
                                <li>Ability to work across multiple time zones</li>
                            </ul>
                            <h5>Educational Requirement:</h5>
                            <ul>
                                <li>Bachelor's degree in business, engineering or related field</li>
                                <li>Minimum of 3 years of prior business analyst experience</li>
                                <li>Formal training in business analysis techniques</li>
                                <li>Minimum of 3 years experience of using, supporting or testing commercial/business applications.</li>
                                <li>Minimum of 2 years experience of working in a software house environment, supporting, designing or testing packaged software</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#dataScientist" aria-expanded="true" aria-controls="dataScientist">Data Scientist</a> </h4> </div>
                    <div id="dataScientist" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>Extract insights and actionable recommendations from large volumes of data.</li>
                                <li>Leverage data mining and machine learning approaches to model and predict end user behavior.</li>
                                <li>Formulate business problems, translate them into data science projects and provide solution approaches.</li>
                                <li>Develop new algorithms and methods for optimizing revenue and key performance metrics.</li>
                                <li>Design experiments to answer targeted questions and communicate informed conclusions and recommendations.</li>
                                <li>Work with cross functional teams to deliver yearly financial goals by implementing, managing, and communicating monetization programs.</li>
                                <li>Inspire the adoption of advanced analytics and data science across different teams and functions.</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>3+ years of experience in solving analytical problems using quantitative approaches.</li>
                                <li>Experience delivering world class data science practices that leverage complex data.</li>
                                <li>Experience with big data technologies such as Hadoop, MapReduce, Mahout, Hive, Pig etc.</li>
                                <li>Hands-on experience using business intelligence and statistical analysis tools such as SQL, SAS, R, SPSS etc.</li>
                                <li>Fluency with at least one scripting language such as Python or PHP.</li>
                                <li>Ability to execute multiple projects and drive key business results under tight deadlines.</li>
                                <li>Self-starter attitude with willingness to learn and master new technologies.</li>
                                <li>Ability to quickly understand business problems, find patterns and insights within structured and unstructured data.</li>
                                <li>Ability to clearly communicate results to technical and non-technical audiences.</li>
                            </ul>
                            <h5>Educational Requirement:</h5>
                            <ul>
                                <li>Ph.D. or Masters' Degree in Operations Research, Applied Statistics, Data Mining and/or related quantitative engineering field.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#salManager" aria-expanded="true" aria-controls="salManager">Sales Manager</a> </h4> </div>
                    <div id="salManager" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>Cold call potential clients to close new business</li>
                                <li>Strengthen relationships with current customers and grow business</li>
                                <li>Maximize profits for company</li>
                                <li>Research quality leads and markets</li>
                                <li>Provide exceptional customer service</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <p>BA/BS Degree or equivalent work experience</p>
                            <ul>
                                <li>2 + years of experience in a sales-related field or industry</li>
                                <li>Working knowledge of Microsoft Word & Microsoft Excel</li>
                                <li>“Can do” attitude</li>
                                <li>Team-player</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#seniorJavaEngineer" aria-expanded="true" aria-controls="seniorJavaEngineer">Senior Java Engineer</a> </h4> </div>
                    <div id="seniorJavaEngineer" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>Understand and translate business needs into technical requirements and implementations producing maintainable, well-documented code.</li>
                                <li>Develop multi-tiered enterprise solutions based on Java 8, JEE, Spring, MVC.</li>
                                <li>Web Services, SOAP, REST, JAX-RS, JAX-WS. XML Technologies including XPath and XSLT.</li>
                                <li>Data Service technologies including session/entity EJBs with CMP, Hibernate, HQL, and JDBC.</li>
                                <li>Oracle, SQL, PL/SQL, Postgres, database design and implementation, ETL a plus</li>
                                <li>HTML5, CSS3, AJAX, JavaScript, Angular.</li>
                                <li>Maven, Ant, Axis, SVN, Git. Test-driven development, SCRUM, Agile.</li>
                                <li>Excellent communication skills, proactive, and resourceful.</li>
                                <li>Self-starter and independent, but appreciates the importance of teamwork.</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>6+ years of commercial server-side development experience for enterprise class or large scale web-based, multi-tenant applications.</li>
                                <li>Solid understanding OOAD and experience in Java 8/JEE programming.</li>
                                <li>Solid experience in data-driven, multithreaded, and distributed web application development.</li>
                                <li>Strong communication skills for working in a team environment.</li>
                                <li>BS or MS in CS or equivalent</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#careersAccordion" href="#seniorQaEngineer" aria-expanded="true" aria-controls="seniorQaEngineer">Senior QA Engineer</a> </h4> </div>
                    <div id="seniorQaEngineer" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h5>Position Responsibilities:</h5>
                            <ul>
                                <li>build a testing process from scratch</li>
                                <li>be the main person who will lead and oversee this process.</li>
                            </ul>
                            <h5>Position Requirements:</h5>
                            <ul>
                                <li>Strong knowledge of client-server architecture</li>
                                <li>Basic knowledge of Javascript/HTML/CSS</li>
                                <li>Deep understanding of HTTP Protocol (considered a plus)</li>
                                <li>Knowledge of SOAP and REST services. Experience in testing of such services.</li>
                                <li>Knowledge of JSON and SOAP+XML data formats.</li>
                                <li>Strong knowledge of SQL</li>
                                <li>Automated testing of web applications (considered a plus)</li>
                                <li>Automated testing of SOAP and REST services (considered a plus)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>