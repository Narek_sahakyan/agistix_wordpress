<?php
get_header();
?>
<!-- banner-section -->
<section class="banner-section section-lg home-banner" style="background-image:url(<?php echo( get_template_directory_uri() ); ?>/static/img/general/banner/home.jpg)">
    <div class="container">
        <h1 class="banner-title spec-title">Achieve total visibility into every transaction, every shipment.</h1> </div>
    <div id="sparkSVG" class="banner-svg-wrapper"></div>
    <p class="banner-desc top-desc" data-text=".top-desc">See real-time data for every inbound and
        <br>outbound transaction across the globe.</p>
    <p class="banner-desc right-desc" data-text=".right-desc">Easily spot transactions that
        <br>don’t meet your standards. </p>
    <p class="banner-desc left-desc" data-text=".left-desc">Quantify the impact of your work—
        <br>and easily share with stakeholders.</p>
</section>
<!-- end banner-section -->
<!-- try-section -->
<section class="try-section">
    <div class="container">
        <h2 class="spec-title">Try before you buy with a free trial.</h2> <a href="<?php echo( home_url( '/trial-offer/' ) ); ?>" class="btn btn-lg btn-transparent">Learn More</a> </div>
</section>
<!-- end try-section -->
<!-- benefits-section -->
<section class="section benefits-section section-light text-center">
    <div class="container">
        <h3 class="section-title">No matter where shipments are coming from, where they’re going, or how they’re traveling: Agistix makes it easy for you to drive results at scale across your entire supply chain.</h3>
        <div class="row">
            <div class="col-sm-6 col-lg-4 heading-col">
                <h3>Drives Enterprise <br> Results</h3>
                <p class="benefits-desc">Join industry leaders and optimize shipping throughout your supply chain with Agistix. Our customers report slashing some costs by 25 percent or more.</p>
                <p><a href="<?php echo( home_url( '/overview/' ) ); ?>" class="btn btn-agx">Learn More</a></p>
            </div>
            <div class="col-sm-6 col-lg-4 heading-col">
                <h3>Easy to <br> Get Started</h3>
                <p class="benefits-desc">Get up and running in just 4-6 weeks. From there, it’s easy to prioritize and quantify improvements to the thousands of shipping decisions your ecosystem makes every day.</p>
                <p><a href="<?php echo( home_url( '/overview/' ) ); ?>" class="btn btn-agx">Learn More</a></p>
            </div>
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-0 heading-col">
                <h3>Works the Way <br> You Work</h3>
                <p class="benefits-desc">Whether your systems and operational processes are simple or sophisticated, Agistix can support and enhance your workflows.</p>
                <p><a href="<?php echo( home_url( '/overview/' ) ); ?>" class="btn btn-agx">Learn More</a></p>
            </div>
        </div>
    </div>
</section>
<!-- end benefits-section -->
<section class="section factioid-section section-gray text-center">
    <div class="container">
        <h3 class="section-title">Get to know Agistix by the numbers. <br>Conquer tough questions about shipments with ease.</h3>
        <div class="row factioid-row">
            <div class="col-sm-4 col-lg-4 billion-col">
                <h3>1 <br>Billion</h3>
                <p>Transactions move
                    <br>through Agistix</p>
            </div>
            <div class="col-sm-4 col-lg-4 second-col">
                <h3>1 <br>Second</h3>
                <p>To look up
                    <br>any shipment</p>
            </div>
            <div class="col-sm-4 col-lg-4 resources-col">
                <h3>0 <br>IT Resources</h3>
                <p>To start benefitting
                    <br>from Agistix</p>
            </div>
        </div>
    </div>
</section>
<section class="section section-light video-section text-center">
    <div class="container">
        <h3 class="section-title">No matter how complex your supply chain, get real-time data <br>on all transactions worldwide—every carrier, every mode.</h3>
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3 heading-col">
                <h3>Spend less time searching </h3>
                <p>Agistix delivers easy answers to the hard questions across your supply chain</p>
                <p><a href="<?php echo( home_url( '/overview/' ) ); ?>" class="btn btn-agx">Learn More</a></p>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="video-wrapper">
                    <video width="400" height="300" poster="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/videoposter.jpg" id="video">
                        <source src="./video/agx.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'> </video>
                    <button class="play-btn" id="videoPlay"><i class="agx-icon-play"></i></button>
                    <p class="video-text">Introducing Agistix</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3 heading-col">
                <h3>Try Agistix <br> for Free</h3>
                <p>See results immediately with our free trial for qualified companies</p>
                <p><a href="<?php echo( home_url( '/trial-offer/' ) ); ?>" class="btn btn-agx">Learn More</a></p>
            </div>
        </div>
    </div>
</section>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>