<?php
/*
Template Name: Trial Offer
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section trialOffer-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section text-center section-light-gray">
        <div class="container">
            <h2 class="section-title">Agisitx has a complimentary trial package of our visibility solution available to select, qualified customers.</h2>
            <p class="section-lead text-left">We've made it easy for you to give the Agistix Visibility solution a try. For select qualified customers, we are offering a free package to help you get up and running on Agistix. It includes:</p>
            <br>
            <ul class="text-left section-lists">
                <li>Integration of up to two data feeds and up to 10 of your highest volume accounts</li>
                <li>Customized and branded dashboards, reports, login and customer tracking app</li>
                <li>One day of consulting from our professional services team, including a one hour debrief of findings after 30 days</li>
            </ul>
        </div>
    </section>
    <section class="section section-light contact-form-section">
        <div class="container">
            <div id="successAlert" class="alert alert-success" style="display:none"> Your comment was sent successfully, thank you for taking interest in Agistix. </div>
            <h2 class="text-left">Contact us to learn more and see if you qualify.</h2>
            <br>
            <div class="row">
                <div class="col-sm-9 col-lg-9">
                    <form class="contact-form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" name="contact_form" id="trialForm">
                        <input type="hidden" name="oid" value="00DE0000000Iufz">
                        <input type="hidden" name="retURL" value="http://www.agistix.com/contact?status=success">
                        <input type="hidden" name="lead_source" value="Website - Trial Offer Page">
                        <input type="hidden" name="Campaign_ID" value="701E00000004oxl">
                        <div class="form-group">
                            <label for="first_name">First Name <span class="req">*</span></label>
                            <input class="form-control" required id="first_name" name="first_name"> </div>
                        <div class="form-group">
                            <label for="last_name">Last Name <span class="req">*</span></label>
                            <input class="form-control" required id="last_name" name="last_name"> </div>
                        <div class="form-group">
                            <label for="company">Company <span class="req">*</span></label>
                            <input class="form-control" required id="company" name="company"> </div>
                        <div class="form-group">
                            <label for="email">Email <span class="req">*</span></label>
                            <input type="email" class="form-control" required id="email" name="email"> </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input class="form-control" id="phone" name="phone"> </div>
                        <div class="form-group">
                            <label for="comments">Message</label>
                            <textarea id="comments" name="comments" rows="5" class="form-control"></textarea>
                        </div>
                        <p>
                            <button class="btn btn-agx" type="submit">Send</button>
                            <button class="btn btn-default" type="reset">Cancel</button>
                        </p>
                    </form>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="address">
                        <h4>Contact Us</h4> <address> <strong>Agistix, Inc.</strong><br> 177 Bovet Road, Suite 110<br> San Mateo, CA 94402<br> <abbr title="Phone">P:</abbr> +1 (888) 244-7849 </address> </div>
                    <div class="emails"> <address> <strong>Email</strong><br> <a href="mailto:info@agistix.com">info@agistix.com</a><br> <a href="mailto:support@agistix.com">support@agistix.com</a><br> <a href="mailto:sales@agistix.com">sales@agistix.com</a> </address> </div>
                </div>
            </div>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>