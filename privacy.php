<?php
/*
Template Name: Privacy
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section privacy-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section section-light privacy-section">
        <div class="container">
            <h2 class="section-title text-center">This privacy policy covers how we treat the information <br>we receive from our visitors and customers.</h2>
            <br>
            <!-- information -->
            <h4>Information</h4>
            <p>Visitors to the <a href="<?php echo( home_url( '/' ) ); ?>">www.agistix.com</a> website are asked to supply contact information (including their name, e-mail address and phone number) as well as additional information such as the visitor's title, company name, number of employees and type of business. You can opt out of providing additional information by not entering it when asked. This contact and additional information is used to communicate with the visitors and to address any questions or comments the visitors may have.</p>
            <p>Agistix provides its service to its customers subject to the agreement by which you have gained access to the shipping services and this Privacy Policy, which shall be deemed part of such agreement. Customers are required to give us contact information (including their name, e-mail address and phone number) as well as information about their company. This information is necessary for the provision of the Agistix service to customers.</p>
            <p>We may also use the information to contact customers and prospects to further discuss their interest in our company, the services we provide and ways we can improve them, and to send information such as announcements of promotions and events regarding our company or partners. We may also e-mail a customer newsletter and updates about the services, Agistix or its partners.</p>
            <p>Agistix does not distribute or share customer e-mail addresses, except as noted in its Terms of Service, or as may be required by law.</p>
            <p>All information that we collect from customers through the Shipment Manager is used solely for purposes of processing the shipment. This information includes the pickup address, recipient's name and address and product descriptions, is used to generate the appropriate documents and is communicated to the freight forwarding company. Our customers can also store the contact information of recipients in an online address book.</p>
            <p>We do not disclose to any third party the information provided, although we may provide this information on an anonymous, aggregated basis to vendors, customers and partners as part of our service offering.</p>
            <p>Information from both visitors and customers is currently being entered into a NetSuite online application. The use of the information we provide to NetSuite is covered by their Privacy Policy and Terms of Service, which can be found at <a href="http://www.netsuite.com">www.netsuite.com</a>.</p>
            <p>Other third parties, such as content providers, may provide information on a Agistix Web site, but they are not permitted to collect any information, nor does Agistix share any personally identifiable user information with these parties.</p>
            <!-- end information -->
            <!-- Technical Issues -->
            <h4>Technical Issues</h4>
            <p>All communication on the Internet takes place between pairs of IP Addresses. Our Web servers will log the IP address that any connection is made from. We may use these IP addresses to help diagnose technical problems.</p>
            <p>Your browser has options to accept, reject, or provide you with notice when a cookie is sent. A "cookie" is a small line of text that is stored with your Web browser for record-keeping purposes and to help us provide better service to you. We use cookies to save your password (in an encrypted format) on your machine so you don't have to re-enter it each time you visit our site. We also use cookies to track your usage of the services.</p>
            <p>Our website may contain links to other Web sites. Agistix is not responsible for the privacy practices or the content of these other Web sites. Customers and visitors must check the policy statement of these others Web sites to understand their policies. Customers and visitors who access a linked site may be disclosing their private information. It is your responsibility to keep such information private and confidential.</p>
            <p>Agistix has security measures in place to help protect against the loss, misuse and alteration of the data under our control. We use a firewall to verify access to our services, and restrict access based upon user names or e-mail addresses, and password.</p>
            <!-- end Technical Issues -->
            <!-- Opt-Out Policy -->
            <h4>Opt-Out Policy</h4>
            <p>Agistix offers you a means to choose how we may use information you provided. If, at any time after registering for information or ordering the Services, you change your mind about receiving information from us or about sharing information with third parties, send us a request specifying your new choice. Send your request to <a href="mailto:privacy@agistix.com">privacy@agistix.com</a>. However, you cannot opt out of communications necessary for the provision of the services, such as communications regarding shipments.</p>
            <!-- end pt-Out Policy -->
            <!-- Correcting and Updating Your Information -->
            <h4>Correcting and Updating Your Information</h4>
            <p>To update or change your information, either e-mail us at <a href="mailto:privacy@agistix.com">privacy@agistix.com</a> or call +1 (888) 244-7849.</p>
            <!-- end Correcting and Updating Your Information -->
            <!-- Additional Information -->
            <h4>Additional Information</h4>
            <p>Questions regarding this Privacy Policy or the practices of this site should be directed to</p>
            <div class="address"> <address> <strong>Agistix, Inc.</strong><br> 177 Bovet Road, Suite 110<br> San Mateo, CA 94402<br> <abbr title="Phone">P:</abbr> +1 (888) 244-7849 </address> </div>
            <div class="emails"> <address> <strong>Email</strong><br> <a href="mailto:privacy@agistix.com">privacy@agistix.com</a> </address> </div>
            <!-- end Additional Information -->
            <!-- Binding Agreement -->
            <h4>Binding Agreement</h4>
            <p>Your use of our site, including any dispute concerning privacy, is subject to this Privacy Policy. By using our services and communicating through our site, you are accepting the practices set out in this Privacy Policy. We reserve the right to modify this Privacy Policy at any time by posting the changes on this page. Please check the revision date at the bottom of this page to determine if the statement has been modified since you last reviewed it. Your continued use of our services or any portion of our site following posting of the updated Privacy Policy will constitute your acceptance of the changes.</p>
            <!-- end Binding Agreement -->
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>