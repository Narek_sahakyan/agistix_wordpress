<?php
get_header();
?>
<section class="blog-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="blog-section-title">
                    <span>Search results</span>
                </h2> 
            </div>

            <div class="blog-banner-img">
                <img class="img-responsive" src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/banner/blogpost3.jpg" alt="">
            </div>

        </div>
    </div>
</section>
<section class="latest-section">
    <div class="container">
        <h2>Search results for "<?php echo( get_search_query() ); ?>"</h2>
        <div class="row">
            <?php
            if ( have_posts() ) {
                ?>
                <div class="col-sm-3 col-sm-push-9">
                    <div class="twitter-block">
                        <div class="search-block">
                            <form action="<?php echo( home_url( '/' ) ); ?>" method="get">
                                <div class="form-group clearfix">
                                    <input type="text" name="s" class="form-control">
                                    <button type="submit" class="btn btn-search">
                                        <i class="agx-icon-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-sm-pull-3">
                    <div class="row">
                        <?php
                        while ( have_posts() ) {
                            the_post();
                            ?>
                            <div class="col-sm-6">
                                <div class="latest-item">
                                    <?php
                                    if ( has_post_thumbnail() ) {
                                    ?>
                                        <a href="<?php the_permalink(); ?>" class="doc-item-img"><?php the_post_thumbnail( 'large' ); ?></a>
                                    <?php
                                    }
                                    ?>
                                    <div class="latest-item-inner">
                                        <h4><?php the_title(); ?></h4>
                                        <p><?php the_excerpt(); ?></p>
                                        <a class="resources-read-more" href="<?php the_permalink(); ?>">
                                            Read more <span>></span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        <?php
                        }

                        echo( paginate_links() );
                    } else {
                    ?>
                        <p>There is no post yet.</p>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>