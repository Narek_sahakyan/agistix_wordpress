<?php
require_once get_template_directory() . '/includes/hooks.php';

function agistix_get_featured_image_src( $post_id, $size = 'thumbnail', $return_sizes = false ) {
    $featured_image_src = '';
    $featured_image_id = get_post_thumbnail_id( $post_id );

    if ( ! empty( $featured_image_id ) ) {
        $featured_image = wp_get_attachment_image_src( $featured_image_id, $size );

        if ( ! empty( $featured_image ) ) {
            if ( $return_sizes ) {
                return $featured_image;
            }

            $featured_image_src = $featured_image[0];
        }
    }

    return $featured_image_src;
}

function agistix_get_resources() {
    $args = array(
        'post_type'         => 'resource',
        'posts_per_page'    => -1,
        'orderby'           => 'menu_order',
        'order'             => 'ASC'
    );

    $resources = get_posts( $args );

    return $resources;
}

function agistix_get_career_resources() {
    $args = array(
        'post_type'         => 'career-resource',
        'posts_per_page'    => -1,
        'orderby'           => 'menu_order',
        'order'             => 'ASC'
    );

    $career_resources = get_posts( $args );

    return $career_resources;
}
?>