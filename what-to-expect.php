<?php
/*
Template Name: What To Expect
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section wtexpect-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <!-- end banner-section -->
    <section class="section section-light text-center">
        <div class="container">
            <h2 class="section-title">Answer the toughest questions with ease.</h2>
            <p class="section-lead"> If you’re using traditional supply chain technology, answering your most important business questions can take hours—time you just don’t have. Why work extra hours searching for data? Agistix puts the insights you need at your fingertips. With just a few clicks, you can analyze your shipping spend, identify areas for improvement, and change the way your management thinks about freight. Next time your executives ask you tough questions, you'll not only give clear answers, but also have enough insight to make a strategic contribution to the business. </p>
        </div>
    </section>
    <section class="section-light-gray datasheet-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4"> <img src="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/datasheet.png" alt="" class="img-responsive datasheet-img"> </div>
                <div class="col-lg-6">
                    <h4 class="datasheet-title">Learn more about supply chain insights with our <strong>Agistix Solution Datasheet.</strong></h4>
                    <br>
                    <p class="text-center"><a href="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/datasheet/Agistix_Solution_Overview.pdf" class="btn btn-lg btn-agx" download>Download Now</a></p>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section>
    <section class="section section-light text-center help-section">
        <div class="container">
            <h2 class="section-title">Agistix helps you figure out:</h2>
            <ul class="help-list">
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-gray center">What is my freight spend across all of my suppliers?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-blue center">What do I owe that I haven’t been invoiced for yet?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-dark-blue center">How does on-time performance compare across all my carriers?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-dark-blue center">How do I report damaged shipments?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-oragne center">Am I being billed for goods that weren’t delivered yet?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-gray center">What's coming inbound today?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-blue center">Can I reduce costs by better leveraging my negotiated contracts?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-gray center">Which suppliers aren’t shipping according to my guidelines?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-dark-blue center">What’s driving the delta between shipping estimates and actuals?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-gray center">Did I get the shipment that I'm being invoiced for?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-ligh-blue center">How much are the contracts I negotiated saving my company?</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="help-content">
                        <div class="content-inner">
                            <div class="table">
                                <p class="text-oragne center">Am I being billed properly based on the contracts I negotiated?</p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>