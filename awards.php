<?php
/*
Template Name: Awards
*/

get_header();

while ( have_posts() ) : the_post();
?>
    <section class="section section-light awards-section">
        <div class="container">
            <h2 class="section-title text-center"><?php the_title(); ?></h2>
            <p><strong>San Mateo, CA, February 24, 2017</strong> – <a href="<?php echo( home_url( '/' ) ); ?>">Agistix</a>, a global innovator specializing in harnessing logistics data, services and solutions to elevate supply chain performance, has been named in the <em>Supply & Demand Chain Executive</em> 2017 Pros to Know Awards. The annual awards recognize supply chain executives and manufacturing and non-manufacturing enterprises that are leading initiatives to help prepare their companies’ supply chains for the significant challenges of today’s business climate.</p>
            <p>“It is an honor to be recognized by <em>Supply & Demand Chain Executive</em> as a company helping our customers to improve the quality and accuracy of their supply chain data, while helping them to create better efficiencies and reduce expenses,” said Trevor Read, CEO of Agistix.</p>
            <p>“As companies increasingly seek to ensure they have access to high quality logistics data to measure, track and assess their performance, we look forward to continuing to deliver worldwide reach, helping our customers find savings and gain visibility into critical value drivers, from enhanced supply chain performance, to proper financial compliance, accurate product profitability and comprehensive cost-to-serve metrics.”</p>
            <p><em>Supply & Demand Chain Executive</em> received more than 350 entries for the 2017 Pros to Know Awards. This year’s list includes the 2017 Provider Pros to Know recognizing individuals from software firms and service providers, consultancies or academia who have helped their supply chain clients or the supply chain community at large prepare to meet these challenges. The awards also highlight Practitioner Pros who do the same within their own companies.</p>
            <h4>About Supply & Demand Chain Executive</h4>
            <p><strong><em>Supply & Demand Chain Executive</em></strong> is the executive’s user manual for successful supply and demand chain transformation, utilizing hard-hitting analysis, viewpoints and unbiased case studies to steer executives and supply management professionals through the complicated, yet critical, world of supply and demand chain enablement to gain competitive advantage. On the Web at <a href="http://www.SDCExec.com">www.SDCExec.com</a>.</p>
            <h4>About Agistix</h4>
            <p>Agistix helps companies with complex supply chains to achieve accurate, real-time visibility of every shipment they pay for across the globe. A full suite of solutions work with a company’s existing systems to deliver deep insight into what's really happening across an entire supply chain. Armed with this insight, supply chain professionals are able to optimize the movement of goods—easily identifying and acting on problems and opportunities across thousands of partners, suppliers, and carriers. Agistix customers have better control of shipping rates, negotiate better contracts, more easily identify fraud, and establish stronger, more strategic supply chains. Agistix, was founded in 2004 and based in San Mateo, California.</p>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>