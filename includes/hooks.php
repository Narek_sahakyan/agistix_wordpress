<?php
add_action( 'after_setup_theme', 'agistix_setup' );

function agistix_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
}

add_action( 'init', 'agistix_init' );

function agistix_init() {
    register_nav_menus(
        array(
            'header-menu'   => esc_html__( 'Header Menu', 'agistix' ),
            'footer-menu'   => esc_html__( 'Footer Menu', 'agistix' )
        )
    );

    register_post_type( 'resource',
        array(
            'labels'        => array(
                    'name'                  => 'Resources',
                    'singular_name'         => 'Resource',
                    'add_new'               => 'Add New',
                    'add_new_item'          => 'Add a New Resource',
                    'edit'                  => 'Edit',
                    'edit_item'             => 'Edit Resource',
                    'new_item'              => 'New Resource',
                    'view'                  => 'View Resources',
                    'view_item'             => 'View Resource',
                    'search_items'          => 'Search Resources',
                    'not_found'             => 'No resource found',
                    'not_found_in_trash'    => 'No resource found in Trash',
            ),
            'public'        => true,
            'has_archive'   => false,
            'supports'      => array( 'title', 'thumbnail' ),
            'menu_icon'     => 'dashicons-media-document'
        )
    );

    register_post_type( 'career-resource',
        array(
            'labels'        => array(
                    'name'                  => 'Career Resources',
                    'singular_name'         => 'Resource',
                    'add_new'               => 'Add New',
                    'add_new_item'          => 'Add a New Resource',
                    'edit'                  => 'Edit',
                    'edit_item'             => 'Edit Resource',
                    'new_item'              => 'New Resource',
                    'view'                  => 'View Resources',
                    'view_item'             => 'View Resource',
                    'search_items'          => 'Search Resources',
                    'not_found'             => 'No resource found',
                    'not_found_in_trash'    => 'No resource found in Trash',
            ),
            'public'        => true,
            'has_archive'   => false, 
            'supports'      => array( 'title', 'thumbnail' ),
            'menu_icon'     => 'dashicons-portfolio'
        )
    );
}

add_action( 'wp_enqueue_scripts', 'agistix_assets' );

function agistix_assets() {
    wp_enqueue_style( 'agistix-style', get_template_directory_uri() . '/static/css/mainad0yho.min.css' );
} 
?>