<!-- section-contact -->
<section class="section-contact">
    <div class="section-content">
        <h2 class="spec-title">Learn how Agistix can improve your business</h2>
        <p class="text-center"><a href="<?php echo( home_url( '/contact/' ) ); ?>" class="btn btn-lg btn-transparent">Learn More</a></p>
        <div class="section-bg"></div>
    </div>
</section>
<!-- end section-contact -->