<?php
/*
Template Name: Resources
*/

get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );
    ?>
    <!-- banner-section -->
    <section class="banner-section resources-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <h1 class="banner-title spec-title"><?php the_title(); ?></h1> </div>
        <div class="banner-backdrop"></div>
    </section>
    <?php
    $resources = agistix_get_resources();

    if ( ! empty( $resources ) ) {
    ?>
        <!-- end banner-section -->
        <section class="section section-light text-center">
            <div class="container">
                <p class="section-lead"> Agistix offers the creation of numerous resources throughout its platform. Browse our resources and learn more about how Agistix can benefit your business or <a href="<?php echo( home_url( '/contact/' ) ); ?>">contact us</a> to talk to a member of our team. </p>
                <div class="slick-slider datasheet-slider" id="datasheetSlider">
                    <?php
                    foreach ( $resources as $resource ) {
                        $resource_image = agistix_get_featured_image_src( $resource->ID, 'large' );

                        if ( empty( $resource_image ) ) {
                            continue;
                        }

                        $pdf = get_field( 'pdf', $resource->ID );

                        if ( empty( $pdf['url'] ) ) {
                            continue;
                        }
                        ?>
                        <div class="slick-item">
                            <div class="datasheet-item"> <img src="<?php echo( esc_url( $resource_image ) ); ?>" class="img-responsive">
                                <div class="datasheet-action">
                                    <a href="" class="datasheet-action-btn pdf-download-btn" data-pdfname="<?php echo( esc_url( $pdf['url'] ) ); ?>" target="_blank" data-toggle="modal" data-target="#reqPdfModal"> <i class="agx-icon-link"></i> </a>
                                </div>
                            </div>
                            <p class="datasheet-desc"><?php echo( $resource->post_title ); ?></p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </section>
    <?php
    }
    ?>
    <section class="section section-light-gray next-section">
        <div class="container">
            <h2 class="section-title">Next Steps</h2>
            <ul class="steps">
                <li>
                    <a href="<?php echo( home_url( '/trial-offer/' ) ); ?>"> <strong>Learn more</strong> about our free trial package </a>
                </li>
                <li>
                    <a href="javascript:;" id="calendlyToggle"> <strong>Schedule</strong> a meeting with one of our experts </a>
                </li>
            </ul>
        </div>
    </section>
    <section class="section section-light taking-notice-section">
        <div class="container">
            <h2 class="section-title text-center">The World is Taking Notice</h2>
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="text-ligh-blue">Supply & Demand Chain Executive Recognizes Agistix in the 2016 Pros to Know Awards</h4>
                    <p>Agistix, a global innovator specializing in harnessing logistics data, services. <a href="<?php echo( home_url( '/awards/' ) ); ?>">Read Now</a></p>
                    <h4 class="text-ligh-blue">White Paper</h4>
                    <p>IBM Institute's new paper "New Rules for a New Decade: A Vision for Smarter Supply Chain Management" speaks to the need for visibility of accurate, time-sensitive information. <a href="<?php echo( get_template_directory_uri() ); ?>/pdf/New_Rules_for_a_New_Decade_IBM_Nov_2010.pdf" download>Download Now</a></p>
                </div>
                <div class="col-lg-6">
                    <h4 class="text-ligh-blue">High Tech: The Rise of SaaS and the Cloud</h4>
                    <p>Agistix's Trevor Read speaks to a customer that gained visibility to 98,000 shipments in three weeks. <a href="http://www.inboundlogistics.com/cms/article/high-tech-the-rise-of-saas-and-the-cloud/" target="_blank">Read Now</a></p>
                    <h4 class="text-ligh-blue">McKinsey Quarterly Report</h4>
                    <p>McKinsey Quarterly describes how building the supply chain of the future means splintered supply chains that dismantle complexity. <a href="http://www.mckinseyquarterly.com/Building_the_supply_chain_of_the_future_2729" alt="Read Now" target="_blank">Read Now</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-light testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <blockquote class="blockquote video-blockquote">
                        <p>“Now we know exactly what we spend on freight - without having to track down information from different systems or carriers.”</p>
                        <footer>XO Communications <cite title="Source Title"></cite></footer>
                    </blockquote>
                </div>
                <div class="col-lg-6 col-lg-offset-1">
                    <div class="testimonial-resource">
                        <div class="video-wrapper">
                            <video width="400" height="300" poster="<?php echo( get_template_directory_uri() ); ?>/static/img/general/sections/videoPoster.jpg" id="video">
                                <source src="<?php echo( get_template_directory_uri() ); ?>/video/agx.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'> </video>
                            <button class="play-btn" id="videoPlay"><i class="agx-icon-play"></i></button>
                            <p class="video-text">Introducing Agistix</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
endwhile;
?>
<?php get_template_part( 'includes/templates/learn-more' ); ?>
<?php get_footer(); ?>