<?php
get_header();

while ( have_posts() ) : the_post();
    $featured_image = agistix_get_featured_image_src( get_the_ID(), 'full' );

    $logo       = get_field( 'logo' );
    $posts      = get_field( 'posts' );
    $videos     = get_field( 'videos' );
    $documents  = get_field( 'documents' );

    if ( ! empty( $posts ) ) {
        $top_post = array_shift( $posts );
    }
    ?>
    <!-- banner-section -->
    <section class="banner-section  resources-new-banner"<?php if ( ! empty( $featured_image ) ) { echo( ' style="background-image:url(' . esc_url( $featured_image ) . ')"' ); } ?>>
        <div class="container">
            <div class="banner-title">
                <h1 class="spec-title">Resources for Carriers </h1>
                <?php
                if ( ! empty( $logo['url'] ) ) {
                ?>
                    <div class="banner-logo">
                        <img src="<?php echo( esc_url( $logo['url'] ) ); ?>" alt="<?php echo( esc_attr( get_the_title() ) ); ?>">
                    </div>
                <?php
                }
                ?>
            </div>
        </div>

    </section>
    <?php
    if ( ! empty( $top_post ) ) {
        $top_post_image = agistix_get_featured_image_src( $top_post->ID, 'large' );
        ?>
        <!-- end banner-section -->
        <section class="top-recources-section">
            <div class="container">
                <div class="row">
                    <?php
                    if ( ! empty( $top_post_image ) ) {
                    ?>
                        <div class="col-sm-7">
                            <div class="top-resources-bg" style="background-image:url('<?php echo( esc_url( $top_post_image ) ); ?>')"></div>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="col-sm-5">
                        <div class="top-resource-info">
                            <h3><span>top resource</span></h3>
                            <h2><?php echo( $top_post->post_title ); ?></h2>
                            <p><?php echo( $top_post->post_excerpt ); ?></p>
                            <a class="read-more" href="<?php echo( get_permalink( $top_post->ID ) ); ?>">Read more <span>></span> </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
    }

    if ( ! empty( $videos ) ) {
    ?>
        <section class="video-section">
            <div class="container">
                <h2 class="recources-new-section-title"><span>videos</span></h2>
                <div class="row">
                    <?php
                    foreach ( $videos as $video ) {
                    ?>
                        <div class="col-sm-3">
                            <div class="video-item">
                                <div class="video-item-top">
                                    <div class="video-wrapper">
                                        <video width="400" height="300" poster="<?php echo( esc_url( $video['poster']['sizes']['medium'] ) ); ?>" id="video" class="video">
                                            <source src="<?php echo( esc_url( $video['video']['url'] ) ); ?>" type='<?php echo( esc_attr( $video['video']['mime_type'] ) ); ?>'>
                                        </video>
                                        <button class="play-btn" id="videoPlay"><i class="agx-icon-play"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="video-item-info">
                                    <p><?php echo( $video['title'] ); ?></p>
                                    <a class="resources-read-more" href="#">
                                        Watch <span>></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="section-link-wrapper">
                    <a href="#">
                        <i class="agx-icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
    <?php
    }

    if ( ! empty( $documents ) ) {
    ?>
        <section class="documentation-sections">
            <div class="container">
                <h2 class="recources-new-section-title"><span>documentation</span></h2>
                <div class="row doc-items-wrapper">
                    <?php
                    foreach ( $documents as $document ) {
                    ?>
                        <div class="col-sm-3">
                            <div class="doc-item">
                                <a href="" class="doc-item-img">
                                    <img class="img-responsive" src="<?php echo( esc_url( $document['image']['sizes']['medium'] ) ); ?>" alt="<?php echo( esc_attr( $document['title'] ) ); ?>">
                                </a>
                                <p>
                                    <?php echo( $document['label'] ); ?>
                                    <br>
                                    <strong><?php echo( $document['title'] ); ?></strong>
                                </p>
                                <a class="resources-read-more" href="#">
                                    view <span>></span>
                                </a>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="section-link-wrapper">
                    <a href="#">
                        <i class="agx-icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
    <?php
    }

    if ( ! empty( $posts ) ) {
        //$latest_post = array_shift( $posts );
        ?>
        <section class="articles-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="articles-banner" style="background-image: url('<?php echo( get_template_directory_uri() ); ?>/static/img/general/banner/rect10.jpg');">
                            <p>Why supply Chain
                                <br>Visibility May be Easier Than
                                <br>You Think</p>
                        </div>
                    </div>
                    <?php
                    if ( ! empty( $posts ) ) {
                        foreach ( $posts as $post ) {
                            $post_image = agistix_get_featured_image_src( $post->ID, 'large' );
                            ?>
                            <div class="col-sm-3">
                                <div class="article-item">
                                    <?php
                                    if ( ! empty( $post_image ) ) {
                                    ?>
                                        <a href="<?php echo( get_permalink( $post->ID ) ); ?>" class="doc-item-img">
                                            <img class="img-responsive" src="<?php echo( esc_url( $post_image ) ); ?>" alt="<?php echo( esc_attr( $post->post_title ) ); ?>">
                                        </a>
                                    <?php
                                    }
                                    ?>
                                    <div class="article-inner">
                                        <p>
                                            <strong><?php echo( $post->post_title ); ?></strong>
                                        </p>
                                        <a class="resources-read-more" href="<?php echo( get_permalink( $post->ID ) ); ?>">
                                            Read more <span>></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
                <div class="section-link-wrapper">
                    <a href="#">
                        <i class="agx-icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
    <?php
    }
    ?>
<?php
endwhile;
?>
<?php get_footer(); ?>